#include "pch.h"
#include "CppUnitTest.h"
#include "../SalesSystem/ComissionCalculator.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SalesSystemTests
{
	TEST_CLASS(ComissionCalculatorTests)
	{
	public:
		TEST_METHOD(Test500Sale5PercentCommission)
		{ 
			ComissionCalculator cal;
			auto sale = 500.0;
			auto commision = 25.00;
			auto actualCommission = cal.Calculate(sale);

			Assert::AreEqual(commision, actualCommission);
		}

		TEST_METHOD(Test10000Sale5PercentCommission)
		{
			ComissionCalculator cal;
			auto sale = 10000.0;
			auto commision = 500.00;
			auto actualCommission = cal.Calculate(sale);

			Assert::AreEqual(commision, actualCommission);
		}

		TEST_METHOD(Test20kSale6PercentCommissionShouldReturn1200)
		{
			ComissionCalculator cal;
			auto sale = 20000.0;
			auto commision = 1200.0;
			auto actualCommission = cal.Calculate(sale);

			Assert::AreEqual(commision, actualCommission);
		}
		TEST_METHOD(Test10k59Sale6PercentCommissionShouldReturn600_03)
		{
			ComissionCalculator cal;
			auto sale = 10000.59;
			auto commision = 600.03;
			auto actualCommission = cal.Calculate(sale);

			Assert::AreEqual(commision, actualCommission);
		}

		TEST_METHOD(Test55_59Sale6PercentCommissionShouldReturn2_77)
		{
			ComissionCalculator cal;
			auto sale = 55.59;
			auto commision = 2.77;
			auto actualCommission = cal.Calculate(sale);

			Assert::AreEqual(commision, actualCommission);
		}
	};
}

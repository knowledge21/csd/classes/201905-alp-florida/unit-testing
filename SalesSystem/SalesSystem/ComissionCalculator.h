#pragma once
class ComissionCalculator
{
public:
	const double SALE_VALUE_UPPER_LIMIT = 10000.00;
	ComissionCalculator();
	~ComissionCalculator();
	double Calculate(double val);
};

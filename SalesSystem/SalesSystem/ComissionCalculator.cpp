#include "ComissionCalculator.h"
#include <Math.h>

ComissionCalculator::ComissionCalculator()
{
}

ComissionCalculator::~ComissionCalculator()
{
}

double ComissionCalculator::Calculate(double val)
{
	double comissionPercentage = 0.05;

	if (val > SALE_VALUE_UPPER_LIMIT)
	{
		comissionPercentage = 0.06;
	}

	return floor((val * comissionPercentage) * 100) / 100;
}